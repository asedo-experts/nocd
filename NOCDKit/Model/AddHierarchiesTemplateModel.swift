import UIKit

public struct AddHierarchiesTemplateModel {
    public let head: String
    public let title: String
    public let description: String
    public let image: String
}
