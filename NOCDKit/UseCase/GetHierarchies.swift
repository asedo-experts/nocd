import Foundation
import os

public protocol GetHierarchies {
    func get(completion: ((ResultGetHierarchiesAPIModel?)->())?)
}

public class GetHierarchiesImpl: GetHierarchies {
    private let apiEndpoint: APIEndpoint
    
    public init(apiEndpoint: APIEndpoint) {
        self.apiEndpoint = apiEndpoint
    }
    
    public func get(completion: ((ResultGetHierarchiesAPIModel?)->())?) {
        apiEndpoint.getHierarchies { hierarchiesAPIModel in
            if let hierarchies = hierarchiesAPIModel {
                completion?(hierarchies)
            }
        }
    }
    
}
