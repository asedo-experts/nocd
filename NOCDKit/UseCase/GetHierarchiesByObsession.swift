import Foundation
import os

public protocol GetHierarchiesByObsession {
    func get(obsessionID: String, completion: ((ResultGetHierarchyByObsessionAPIModel?)->())?)
}

public class GetHierarchiesByObsessionImpl: GetHierarchiesByObsession {
    private let apiEndpoint: APIEndpoint
    
    public init(apiEndpoint: APIEndpoint) {
        self.apiEndpoint = apiEndpoint
    }
    
    public func get(obsessionID: String, completion: ((ResultGetHierarchyByObsessionAPIModel?)->())?) {
        apiEndpoint.getHierarchiesByObsession(obsessionID: obsessionID) { hierarchiesAPIModel in
            if let hierarchies = hierarchiesAPIModel {
                completion?(hierarchies)
            }
        }
    }
    
}
