import Foundation
import os

public protocol EditCompulsion {
    func edit(compulsion: String, obsessionID: String, compulsionID: String, completion: ((ResultEditCompulsionAPIModel?)->())?)
}

public class EditCompulsionImpl: EditCompulsion {
    private let apiEndpoint: APIEndpoint
    
    public init(apiEndpoint: APIEndpoint) {
        self.apiEndpoint = apiEndpoint
    }
    
    public func edit(compulsion: String, obsessionID: String, compulsionID: String, completion: ((ResultEditCompulsionAPIModel?)->())?) {
        apiEndpoint.editCompulsion(compulsion: compulsion, obsessionID: obsessionID, compulsionID: compulsionID) { compulsion in
            if let compulsion = compulsion {
                completion?(compulsion)
            }
        }
    }
    
}
