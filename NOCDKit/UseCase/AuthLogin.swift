import Foundation
import os

public protocol AuthLogin {
    func login(email: String, pass: String, completion: ((ResultAuthLoginAPIModel?) -> ())?)
}

public class AuthLoginImpl: AuthLogin {
    private let apiEndpoint: APIEndpoint
    
    public init(apiEndpoint: APIEndpoint) {
        self.apiEndpoint = apiEndpoint
    }
    
    public func login(email: String, pass: String, completion: ((ResultAuthLoginAPIModel?)->())?) {
        apiEndpoint.login(email: email, pass: pass) { authModel in
            if let auth = authModel {
                UserDefaults.standard.set(auth.accessToken, forKey: kNOCDToken)
            }
            completion?(authModel)
        }
    }
    
}
