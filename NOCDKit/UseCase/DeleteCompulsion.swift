import Foundation
import os

public protocol DeleteCompulsion {
    func delete(compulsionID: String, completion: ((ResultDeleteCompulsionAPIModel?)->())?)
}

public class DeleteCompulsionImpl: DeleteCompulsion {
    private let apiEndpoint: APIEndpoint
    
    public init(apiEndpoint: APIEndpoint) {
        self.apiEndpoint = apiEndpoint
    }
    
    public func delete(compulsionID: String, completion: ((ResultDeleteCompulsionAPIModel?)->())?) {
        apiEndpoint.deleteCompulsion(compulsionID: compulsionID) { compulsion in
            if let compulsion = compulsion {
                completion?(compulsion)
            }
        }
    }
    
}
