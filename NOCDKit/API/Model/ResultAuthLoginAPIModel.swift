import Foundation

public struct ResultAuthLoginAPIModel: Codable {
    public let accessToken: String
    public let needsPasswordReset: Bool
    public let showSurveyOnboardingV2Prompt: Bool
    public let userInfo: ResultAuthLoginAPIUserInfoModel
    
    enum CodingKeys: String, CodingKey {
        case accessToken, userInfo
        case needsPasswordReset = "needs_password_reset"
        case showSurveyOnboardingV2Prompt = "show_survey_onboarding_v2_prompt"
    }
}

public struct ResultAuthLoginAPIUserInfoModel: Codable {
    public let avatar: String
    public let email: String
    public let guid: String
    public let handle: String
    public let userID: Int
    
    enum CodingKeys: String, CodingKey {
        case avatar, email, guid, handle
        case userID = "user_id"
    }
}
