import Foundation

public struct ResultEditCompulsionAPIModel: Codable {
    public let compulsion: String
    public let obsessionID: String
    public let defaultID: String
    public let compulsionID: String
    
    enum CodingKeys: String, CodingKey {
        case compulsion, obsessionID, defaultID, compulsionID
    }
}
