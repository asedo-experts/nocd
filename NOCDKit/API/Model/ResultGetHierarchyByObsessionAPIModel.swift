import Foundation

public struct ResultGetHierarchyByObsessionAPIModel: Codable {
    public let compulsions: [ResultGetHierarchyByObsessionCompulsionsAPIModel]
    public let defaultID: String
    public let obsession: String
    public let obsessionID: String
    public let subtypeID: String
    public let triggers: [ResultGetHierarchyByObsessionTriggersAPIModel]
    public let useTriggerSOSAnxiety: Bool
    
    enum CodingKeys: String, CodingKey {
        case compulsions, defaultID, obsession, obsessionID, subtypeID, triggers, useTriggerSOSAnxiety
    }
}

public struct ResultGetHierarchyByObsessionCompulsionsAPIModel: Codable {
    public let compulsion: String
    public let compulsionID: String
    public let defaultID: String
    public let obsessionID: String
    
    enum CodingKeys: String, CodingKey {
        case compulsion, compulsionID, defaultID, obsessionID
    }
}

public struct ResultGetHierarchyByObsessionTriggersAPIModel: Codable {
    public let anxiety: Int
    public let defaultID: String
    public let exposures: [ResultGetHierarchyByObsessionTriggersExposuresAPIModel]
    public let obsessionID: String
    public let preventionMessages: [ResultGetHierarchyByObsessionTriggersPreventionMessagesAPIModel]
    public let sosAnxiety: Int
    public let trigger: String
    public let triggerID: String
    
    enum CodingKeys: String, CodingKey {
        case anxiety, defaultID, exposures, obsessionID, preventionMessages, sosAnxiety, trigger, triggerID
    }
}

public struct ResultGetHierarchyByObsessionTriggersExposuresAPIModel: Codable {
    public let anxiety: Int
    public let currentERP: Bool
    public let defaultID: String
    public let exposure: String
    public let exposureID: String
    public let obsessionID: String
    public let triggerID: String
    
    enum CodingKeys: String, CodingKey {
        case anxiety, currentERP, defaultID, exposure, exposureID, obsessionID, triggerID
    }
}

public struct ResultGetHierarchyByObsessionTriggersPreventionMessagesAPIModel: Codable {
    public let message: String
    public let messageID: String
    public let obsessionID: String
    public let triggerID: String
    
    enum CodingKeys: String, CodingKey {
        case message, messageID, obsessionID, triggerID
    }
}
