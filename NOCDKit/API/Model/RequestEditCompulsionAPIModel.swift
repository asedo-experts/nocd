import Foundation

public struct RequestEditCompulsionAPIModel: Codable {
    public let compulsion: String
    public let obsessionID: String
    public let compulsionID: String
    
    enum CodingKeys: String, CodingKey {
        case compulsion, obsessionID, compulsionID
    }
}
