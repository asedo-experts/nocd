import Foundation

public struct RequestDeleteCompulsionAPIModel: Codable {
    public let compulsionID: String
    
    enum CodingKeys: String, CodingKey {
        case compulsionID
    }
}
