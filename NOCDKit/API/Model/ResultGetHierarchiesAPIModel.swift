import Foundation

public struct ResultGetHierarchiesAPIModel: Codable {
    public let obsessions: [ResultGetHierarchiesObsessionAPIModel]?
    
    enum CodingKeys: String, CodingKey {
        case obsessions
    }
}

public struct ResultGetHierarchiesObsessionAPIModel: Codable {
    public let defaultID: String
    public let obsession: String
    public let obsessionID: String
    public let subtypeID: String
    
    enum CodingKeys: String, CodingKey {
        case defaultID, obsession, obsessionID, subtypeID
    }
}
