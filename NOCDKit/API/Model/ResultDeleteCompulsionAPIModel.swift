import Foundation

public struct ResultDeleteCompulsionAPIModel: Codable {
    public let compulsionID: String
    
    enum CodingKeys: String, CodingKey {
        case compulsionID
    }
}
