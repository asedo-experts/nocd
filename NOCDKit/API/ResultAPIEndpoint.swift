import Foundation
import Alamofire

public protocol APIEndpoint {
    func login(email: String, pass: String, completion: @escaping (ResultAuthLoginAPIModel?)->())
    func getHierarchies(completion: @escaping (ResultGetHierarchiesAPIModel?)->())
    func getHierarchiesByObsession(obsessionID: String, completion: @escaping (ResultGetHierarchyByObsessionAPIModel?)->())
    func editCompulsion(compulsion: String, obsessionID: String, compulsionID: String, completion: @escaping (ResultEditCompulsionAPIModel?)->())
    func deleteCompulsion(compulsionID: String, completion: @escaping (ResultDeleteCompulsionAPIModel?)->())
}

class APIEndpointImpl: APIEndpoint {
    
    public func login(email: String, pass: String, completion: @escaping (ResultAuthLoginAPIModel?)->()) {
        let params = ["email" : email, "password" : pass]
        AF
            .request("\(kAPIEndpoint)/v1/login",
                     method: .post,
                     parameters: params,
                     encoding: JSONEncoding.default)
            .response { result in
                print("RESULT: \(result)")
                guard let data = result.data else { return }
                do {
                    let contentModel = try JSONDecoder().decode(ResultAuthLoginAPIModel.self, from: data)
                    print("Model: \(contentModel)")
                    completion(contentModel)
                } catch {
                    print("Error: \(error.localizedDescription)")
                    completion(nil)
                }
        }
    }
    
    public func getHierarchies(completion: @escaping (ResultGetHierarchiesAPIModel?)->()) {
        AF
            .request("\(kAPIEndpoint)/v1/get_obsessions",
                     method: .get,
                     encoding: JSONEncoding.default,
                     headers: authorizedHeaders())
            .response { result in
                print("RESULT: \(result)")
                guard let data = result.data else { return }
                do {
                    let contentModel = try JSONDecoder().decode(ResultGetHierarchiesAPIModel.self, from: data)
                    print("Model: \(contentModel)")
                    completion(contentModel)
                } catch {
                    print("Error: \(error.localizedDescription)")
                    completion(nil)
                }
        }
    }
    
    public func getHierarchiesByObsession(obsessionID: String, completion: @escaping (ResultGetHierarchyByObsessionAPIModel?)->()) {
        AF
            .request("\(kAPIEndpoint)/v1/hierarchy_by_obsession?obsessionID=\(obsessionID)",
                     method: .get,
                     encoding: JSONEncoding.default,
                     headers: authorizedHeaders())
            .response { result in
                print("RESULT: \(result)")
                guard let data = result.data else { return }
                do {
                    let contentModel = try JSONDecoder().decode(ResultGetHierarchyByObsessionAPIModel.self, from: data)
                    print("Model: \(contentModel)")
                    completion(contentModel)
                } catch {
                    print("Error: \(error.localizedDescription)")
                    completion(nil)
                }
        }
    }
    
    public func editCompulsion(compulsion: String, obsessionID: String, compulsionID: String, completion: @escaping (ResultEditCompulsionAPIModel?)->()) {
        let params = RequestEditCompulsionAPIModel(compulsion: compulsion,
                                                   obsessionID: obsessionID,
                                                   compulsionID: compulsionID)
        AF
            .request("\(kAPIEndpoint)/v1/edit_compulsion",
                     method: .post,
                     parameters: params.dictionary,
                     encoding: JSONEncoding.default,
                     headers: authorizedHeaders())
            .response { result in
                print("RESULT: \(result)")
                guard let data = result.data else { return }
                do {
                    let contentModel = try JSONDecoder().decode(ResultEditCompulsionAPIModel.self, from: data)
                    print("Model: \(contentModel)")
                    completion(contentModel)
                } catch {
                    print("Error: \(error.localizedDescription)")
                    completion(nil)
                }
        }
    }
    
    public func deleteCompulsion(compulsionID: String, completion: @escaping (ResultDeleteCompulsionAPIModel?)->()) {
        let params = RequestDeleteCompulsionAPIModel(compulsionID: compulsionID)
        AF
            .request("\(kAPIEndpoint)/v1/delete_compulsion",
                     method: .post,
                     parameters: params.dictionary,
                     encoding: JSONEncoding.default,
                     headers: authorizedHeaders())
            .response { result in
                print("RESULT: \(result)")
                guard let data = result.data else { return }
                do {
                    let contentModel = try JSONDecoder().decode(ResultDeleteCompulsionAPIModel.self, from: data)
                    print("Model: \(contentModel)")
                    completion(contentModel)
                } catch {
                    print("Error: \(error.localizedDescription)")
                    completion(nil)
                }
        }
    }
    
    private func authorizedHeaders() -> HTTPHeaders {
        var headers: HTTPHeaders = ["Content-Type" : "application/json"]
        if let accessToken = UserDefaults.standard.value(forKey: kNOCDToken) as? String {
            headers.add(name: "Authorization", value: accessToken)
        }
        return headers
    }
}

