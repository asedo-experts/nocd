import Foundation

public class NOCDKitCompositionRoot {
    public static var sharedInstance: NOCDKitCompositionRoot = NOCDKitCompositionRoot()

    public static func nuke() {
        sharedInstance = NOCDKitCompositionRoot()
    }
    
    private init() { }
    
    // MARK: Api.
    
    public lazy var resolveAPIEndpoint: APIEndpoint = {
        APIEndpointImpl()
    }()

    // MARK: Use cases.

    public lazy var resolveAuthLogin: AuthLogin = {
        AuthLoginImpl(apiEndpoint: resolveAPIEndpoint)
    }()
    
    public lazy var resolveGetHierarchies: GetHierarchies = {
        GetHierarchiesImpl(apiEndpoint: resolveAPIEndpoint)
    }()
    
    public lazy var resolveGetHierarchiesByObsession: GetHierarchiesByObsession = {
       GetHierarchiesByObsessionImpl(apiEndpoint: resolveAPIEndpoint)
    }()
    
    public lazy var resolveAddHierarchiesTemplatesDataSource: AddHierarchiesTemplatesDataSource = {
        AddHierarchiesTemplatesDataSource.shared
    }()
    
    public lazy var resolveEditCompulsion: EditCompulsion = {
        return EditCompulsionImpl(apiEndpoint: resolveAPIEndpoint)
    }()
    
    public lazy var resolveDeleteCompulsion: DeleteCompulsion = {
        return DeleteCompulsionImpl(apiEndpoint: resolveAPIEndpoint)
    }()

}
