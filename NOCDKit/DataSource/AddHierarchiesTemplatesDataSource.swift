import Foundation

public class AddHierarchiesTemplatesDataSource {
    
    public static var shared: AddHierarchiesTemplatesDataSource = AddHierarchiesTemplatesDataSource()
    
    private init() {}
    
    public var dataSource: [AddHierarchiesTemplateModel] = [AddHierarchiesTemplateModel(head: "HARM OCD",
                                                                                        title: "I could hurt myself or others, or bad things will happen to me.",
                                                                                        description: "Intrusive thoughts about hurting others, including loved one.",
                                                                                        image: "harm-ocd"),
                                                            AddHierarchiesTemplateModel(head: "RELATIONSHIP OCD",
                                                                                        title: "Is this the right person for me?",
                                                                                        description: "Constantly questioning your romantic relationship",
                                                                                        image: "relationship-ocd"),
                                                            AddHierarchiesTemplateModel(head: "CONTAMINATION OCD",
                                                                                        title: "I’m gonna catch this disease",
                                                                                        description: "I’m gonna catch this disease",
                                                                                        image: "contamination OCD icon"),
                                                            AddHierarchiesTemplateModel(head: "SEXUAL ORIENTATION OCD",
                                                                                        title: "Am I gay or straight?",
                                                                                        description: "Obsessing or constantly questioning your sexuality",
                                                                                        image: "sexual-orientation-ocd"),
                                                            AddHierarchiesTemplateModel(head: "RELIGIOUS OCD",
                                                                                        title: "What if god punishes me",
                                                                                        description: "Obsessions that are based in religion, religious beliefs, morality, etc.",
                                                                                        image: "religious-ocd"),
                                                            AddHierarchiesTemplateModel(head: "“JUST RIGHT” OCD",
                                                                                        title: "This just doesn't feel right",
                                                                                        description: "Also known as perfectionism OCD",
                                                                                        image: "just-right-ocd")
    ]
    
}
