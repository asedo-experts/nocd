import Foundation
import NOCDKit


protocol SignInView {
    func showLoader(text: String)
    func hideLoader()
}

class SignInViewModel: ViewModel {
    
    private let email = "Co9373@nocdhelp.com"
    private let pass = "Co9373nocd"
        
    let view: SignInView
    let authLogin: AuthLogin
    
    init(view: SignInView, authLogin: AuthLogin) {
        self.view = view
        self.authLogin = authLogin
    }
    
    override func viewWillAppear() {
        super.viewWillAppear()
        login()
    }
    
    private func login() {
        self.view.showLoader(text: "Authorization...")
        authLogin.login(email: email, pass: pass) { authModel in
            self.view.hideLoader()
            if let _ = authModel {
                Coordinator.shared.goToHierarchiesViewController()
            }
        }
    }
    
}
