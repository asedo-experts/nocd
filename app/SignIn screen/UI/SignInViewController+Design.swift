import UIKit

extension SignInViewController {
    
    func applyDesign() {
        view.backgroundColor = .white
        imageView = getImageView()
        loadingView = getLoadingView()
    }
    
    private func getImageView(_ block: ((UIImageView) -> Void)? = nil) -> UIImageView {
        let imageView = UIImageView()
        imageView.image = Asset.signInBackground.image
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(imageView, constraints: [
            constraint(\.topAnchor),
            constraint(\.bottomAnchor),
            constraint(\.leadingAnchor),
            constraint(\.trailingAnchor)
        ])
        
        block?(imageView)
        return imageView
    }
    
    private func getLoadingView(_ block: ((LoadingAnimationView) -> Void)? = nil) -> LoadingAnimationView {
        let loadingView = LoadingAnimationView()
        loadingView.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(loadingView, constraints: [
            constraint(\.centerXAnchor),
            constraint(\.centerYAnchor),
            constraint(\.heightAnchor, multiplier: 12/100),
            constraint(\.widthAnchor, \.heightAnchor, multiplier: 12/100),
        ])
        
        block?(loadingView)
        return loadingView
    }
    
}
