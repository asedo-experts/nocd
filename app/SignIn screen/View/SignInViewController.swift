import UIKit

class SignInViewController: BaseViewController<SignInViewModel>, SignInView {

    var imageView: UIImageView!
    var loadingView: LoadingAnimationView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        applyDesign()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
       self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    func showLoader(text: String) {
        loadingView.showLoader(text: text)
    }
    
    func hideLoader() {
        loadingView.hideLoader()
    }
    
    
}
