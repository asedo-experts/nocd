import UIKit


protocol HierarchyByObsessionTypeCollectionViewCellProtocol: UICollectionViewCell {
    func configure(with title: String, header: String?, color: UIColor?)
}

class HierarchyByObsessionCollectionViewCell: UICollectionViewCell {
    
    var titleLabel: UILabel!
    var descriptionLabel: UILabel!
    var collectionView: UICollectionView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        applyDesign()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func applyDesign() {
        backgroundColor = .clear
        
        self.titleLabel = self.getTitleLabel({ titleLabel in
            self.descriptionLabel = self.getDescriptionLabel(leadingView: titleLabel, { descriptionLabel in
                self.collectionView = self.getCollectionView(topView: descriptionLabel)
            })
        })
    }
    
    func configure(with vc: UIViewController, title: String, description: String) {
        self.titleLabel.text = title
        self.descriptionLabel.text = description
        
        self.collectionView.delegate = vc as? UICollectionViewDelegate
        self.collectionView.dataSource = vc as? UICollectionViewDataSource
        self.collectionView.reloadData()
    }
    
    private func getTitleLabel(_ block: ((UILabel) -> Void)? = nil) -> UILabel {
        let label = UILabel()
        label.text = "Trigger"
        label.font = UIFont.boldSystemFont(ofSize: 24.0)
        label.textColor = ColorName.black
        label.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(label, constraints: [
            constraint(\.safeAreaLayoutGuide.topAnchor, constant: 32.0),
            constraint(\.leadingAnchor, constant: 16.0),
            constraint(\.trailingAnchor, constraintRelation: .lessThanOrEqual, constant: -16.0)
        ])
        
        block?(label)
        return label
    }
    
    private func getDescriptionLabel(leadingView: UIView, _ block: ((UILabel) -> Void)? = nil) -> UILabel {
        let label = UILabel()
        label.text = "People, things, and places that set off your obsession"
        label.font = UIFont.systemFont(ofSize: 14.0)
        label.textColor = ColorName.black
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        label.alpha = 0.6
        
        addSubview(label, constraints: [
            constraint(\.trailingAnchor, constraintRelation: .lessThanOrEqual, constant: -16.0)
        ])
        
        label.constrainToView(leadingView, constraints: [
            constraint(\.leadingAnchor),
            constraint(\.topAnchor, \.bottomAnchor, constant: 8.0)
        ])
        
        block?(label)
        return label
    }
    
    private func getCollectionView(topView: UIView, _ block: ((UICollectionView) -> Void)? = nil) -> UICollectionView {
        let collectionViewLayout = UICollectionViewFlowLayout()
        collectionViewLayout.minimumLineSpacing = 1
        collectionViewLayout.minimumInteritemSpacing = 1
        
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewLayout)
        collectionView.tag = 110
        collectionView.backgroundColor = .clear
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        
        collectionView.register(HierarchyByObsessionAddCollectionViewCell.self,
                                forCellWithReuseIdentifier: kHierarchyByObsessionAddIdentifier)
        collectionView.register(HierarchyByObsessionWithPointCollectionViewCell.self,
                                forCellWithReuseIdentifier: kHierarchyByObsessionWithPointIdentifier)
        collectionView.register(HierarchyByObsessionSimpleCollectionViewCell.self,
                                forCellWithReuseIdentifier: kHierarchyByObsessionSimpleIdentifier)
        collectionView.register(HierarchyByObsessionWithTitleAndPointCollectionViewCell.self,
                                forCellWithReuseIdentifier: kHierarchyByObsessionWithTitleAndPointIdentifier)
        
        addSubview(collectionView, constraints: [
            constraint(\.trailingAnchor),
            constraint(\.leadingAnchor),
            constraint(\.heightAnchor, multiplier: 0.625)
        ])
        
        collectionView.constrainToView(topView, constraints: [
            constraint(\.topAnchor, \.bottomAnchor, constant: 24.0)
        ])
        
        block?(collectionView)
        return collectionView
    }
    
}
