import UIKit

class HierarchyByObsessionWithPointCollectionViewCell: UICollectionViewCell, HierarchyByObsessionTypeCollectionViewCellProtocol {
    
    var titleLabel: UILabel!
    var colorView: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        applyDesign()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(with title: String, header: String?, color: UIColor? = ColorName.circleRed) {
        self.titleLabel.text = title
        self.colorView.backgroundColor = color
    }
    
    func applyDesign() {
        backgroundColor = .white
        
        self.colorView = self.getColorView({ colorView in
            self.titleLabel = self.getTitleLabel(leadingView: colorView)
        })
    }
    
    private func getColorView(_ block: ((UIView) -> Void)? = nil) -> UIView {
        let kColorViewRadius: CGFloat = 11.0
        let colorView = UIView(frame: CGRect(x: 0, y: 0, width: kColorViewRadius, height: kColorViewRadius))
        colorView.circleCornerRadius()
        colorView.backgroundColor = ColorName.circleRed
        colorView.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(colorView, constraints: [
            constraint(\.centerYAnchor),
            constraint(\.leadingAnchor, constant: 32.0),
        ])
        
        let heightConstraint = NSLayoutConstraint(item: colorView,
                                                  attribute: .height,
                                                  relatedBy: .equal,
                                                  toItem: nil,
                                                  attribute: .notAnAttribute,
                                                  multiplier: 1.0,
                                                  constant: kColorViewRadius)
        let widthConstraint = NSLayoutConstraint(item: colorView,
                                                 attribute: .width,
                                                 relatedBy: .equal,
                                                 toItem: nil,
                                                 attribute: .notAnAttribute,
                                                 multiplier: 1.0,
                                                 constant: kColorViewRadius)
        colorView.addConstraints([heightConstraint, widthConstraint])
        
        block?(colorView)
        return colorView
    }
    
    private func getTitleLabel(leadingView: UIView, _ block: ((UILabel) -> Void)? = nil) -> UILabel {
        let label = UILabel()
        label.text = "Going on social media"
        label.font = UIFont.systemFont(ofSize: 18.0)
        label.textColor = ColorName.black
        label.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(label, constraints: [
            constraint(\.trailingAnchor, constraintRelation: .lessThanOrEqual, constant: -16.0)
        ])
        
        label.constrainToView(leadingView, constraints: [
            constraint(\.leadingAnchor, \.trailingAnchor, constant: 16.0),
            constraint(\.centerYAnchor)
        ])
        
        block?(label)
        return label
    }
    
}
