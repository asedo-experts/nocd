import UIKit

class HierarchyByObsessionWithTitleAndPointCollectionViewCell: UICollectionViewCell, HierarchyByObsessionTypeCollectionViewCellProtocol {
    
    var headerLabel: UILabel!
    var titleLabel: UILabel!
    var colorView: UIView!
    var currentERPButton: UIButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        applyDesign()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(with title: String, header: String?, color: UIColor? = ColorName.circleYellow) {
        self.headerLabel.text = header
        self.titleLabel.text = title
        self.colorView.backgroundColor = color
    }
    
    func applyDesign() {
        backgroundColor = .white
        
        self.headerLabel = self.getHeaderLabel({ headerLabel in
            self.currentERPButton = self.getCurrentERPButton({ currentERPButton in
                self.titleLabel = self.getTitleLabel(topView: headerLabel, trailingView: currentERPButton, { titleLabel in
                    self.colorView = self.getColorView(topView: headerLabel, trailingView: titleLabel)
                })
            })
        })
        
    }
    
    private func getHeaderLabel(_ block: ((UILabel) -> Void)? = nil) -> UILabel {
        let label = UILabel()
        label.text = "Going on social media"
        label.font = UIFont.boldSystemFont(ofSize: 18.0)
        label.textColor = ColorName.black
        label.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(label, constraints: [
            constraint(\.topAnchor, constant: 24.0),
            constraint(\.leadingAnchor, constant: 40.0),
            constraint(\.trailingAnchor, constraintRelation: .lessThanOrEqual, constant: -40.0)
        ])
        
        block?(label)
        return label
    }
    
    private func getCurrentERPButton( _ block: ((UIView) -> Void)? = nil) -> UIButton {
        let button = UIButton()
        button.setTitle("Current ERP", for: .normal)
        button.setTitleColor(ColorName.bondiBlue, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14.0)
        button.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(button, constraints: [
            constraint(\.centerYAnchor),
            constraint(\.trailingAnchor, constant: -24.0)
        ])
        
        block?(button)
        return button
    }
    
    private func getColorView(topView: UIView, trailingView: UIView, _ block: ((UIView) -> Void)? = nil) -> UIView {
        let kColorViewRadius: CGFloat = 11.0
        let colorView = UIView(frame: CGRect(x: 0, y: 0, width: kColorViewRadius, height: kColorViewRadius))
        colorView.circleCornerRadius()
        colorView.backgroundColor = ColorName.circleRed
        colorView.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(colorView)
        
        colorView.constrainToView(topView, constraints: [
            constraint(\.leadingAnchor),
        ])
        
        colorView.constrainToView(trailingView, constraints: [
            constraint(\.centerYAnchor),
            constraint(\.trailingAnchor, \.leadingAnchor, constant: -16.0),
        ])
        
        let heightConstraint = NSLayoutConstraint(item: colorView,
                                                  attribute: .height,
                                                  relatedBy: .equal,
                                                  toItem: nil,
                                                  attribute: .notAnAttribute,
                                                  multiplier: 1.0,
                                                  constant: kColorViewRadius)
        let widthConstraint = NSLayoutConstraint(item: colorView,
                                                 attribute: .width,
                                                 relatedBy: .equal,
                                                 toItem: nil,
                                                 attribute: .notAnAttribute,
                                                 multiplier: 1.0,
                                                 constant: kColorViewRadius)
        colorView.addConstraints([heightConstraint, widthConstraint])
        
        block?(colorView)
        return colorView
    }
    
    private func getTitleLabel(topView: UIView, trailingView: UIView, _ block: ((UILabel) -> Void)? = nil) -> UILabel {
        let label = UILabel()
        label.text = "Record yourself describing the better relationship you could be in"
        label.font = UIFont.systemFont(ofSize: 18.0)
        label.textColor = ColorName.black
        label.numberOfLines = 5
        label.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(label)
        
        label.constrainToView(trailingView, constraints: [
            constraint(\.trailingAnchor, \.leadingAnchor, constraintRelation: .lessThanOrEqual, constant: -16.0)
        ])
        
        label.constrainToView(topView, constraints: [
            constraint(\.topAnchor, \.bottomAnchor, constant: 16.0)
        ])
        
        block?(label)
        return label
    }
    
}
