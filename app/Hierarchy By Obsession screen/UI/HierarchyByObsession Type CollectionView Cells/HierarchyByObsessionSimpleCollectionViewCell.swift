import UIKit

class HierarchyByObsessionSimpleCollectionViewCell: UICollectionViewCell, HierarchyByObsessionTypeCollectionViewCellProtocol {
    
    var titleLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        applyDesign()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(with title: String, header: String?, color: UIColor?) {
        self.titleLabel.text = title
    }
    
    
    func applyDesign() {
        backgroundColor = .white
        
        self.titleLabel = self.getTitleLabel()
    }
    
    private func getTitleLabel(_ block: ((UILabel) -> Void)? = nil) -> UILabel {
        let label = UILabel()
        label.text = "Asking other people if I’m in the right relationship"
        label.font = UIFont.systemFont(ofSize: 18.0)
        label.textColor = ColorName.black
        label.numberOfLines = 3
        label.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(label, constraints: [
            constraint(\.trailingAnchor, constraintRelation: .lessThanOrEqual, constant: -32.0),
            constraint(\.leadingAnchor, constant: 32.0),
            constraint(\.centerYAnchor)
        ])
        
        block?(label)
        return label
    }

    
}
