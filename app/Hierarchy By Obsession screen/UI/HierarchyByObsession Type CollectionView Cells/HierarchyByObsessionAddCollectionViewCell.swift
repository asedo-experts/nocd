import UIKit

class HierarchyByObsessionAddCollectionViewCell: UICollectionViewCell {
    
    var titleLabel: UILabel!
    var plusImageView: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        applyDesign()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func applyDesign() {
        backgroundColor = .white
        
        self.plusImageView = self.getPlusImageView({ colorView in
            self.titleLabel = self.getTitleLabel(leadingView: colorView)
        })
    }
    
    private func getPlusImageView(_ block: ((UIImageView) -> Void)? = nil) -> UIImageView {
        let kPlusImageViewRadius: CGFloat = 18.0
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: kPlusImageViewRadius, height: kPlusImageViewRadius))
        imageView.image = Asset.plus.image
        imageView.tintColor = ColorName.bondiBlue
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(imageView, constraints: [
            constraint(\.centerYAnchor),
            constraint(\.leadingAnchor, constant: 32.0),
        ])
        
        let heightConstraint = NSLayoutConstraint(item: imageView,
                                                  attribute: .height,
                                                  relatedBy: .equal,
                                                  toItem: nil,
                                                  attribute: .notAnAttribute,
                                                  multiplier: 1.0,
                                                  constant: kPlusImageViewRadius)
        let widthConstraint = NSLayoutConstraint(item: imageView,
                                                 attribute: .width,
                                                 relatedBy: .equal,
                                                 toItem: nil,
                                                 attribute: .notAnAttribute,
                                                 multiplier: 1.0,
                                                 constant: kPlusImageViewRadius)
        imageView.addConstraints([heightConstraint, widthConstraint])
        
        block?(imageView)
        return imageView
    }
    
    private func getTitleLabel(leadingView: UIView, _ block: ((UILabel) -> Void)? = nil) -> UILabel {
        let label = UILabel()
        label.text = "Add a trigger"
        label.font = UIFont.systemFont(ofSize: 18.0)
        label.textColor = ColorName.bondiBlue
        label.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(label, constraints: [
            constraint(\.trailingAnchor, constraintRelation: .lessThanOrEqual, constant: -16.0)
        ])
        
        label.constrainToView(leadingView, constraints: [
            constraint(\.leadingAnchor, \.trailingAnchor, constant: 16.0),
            constraint(\.centerYAnchor)
        ])
        
        block?(label)
        return label
    }
    
}
