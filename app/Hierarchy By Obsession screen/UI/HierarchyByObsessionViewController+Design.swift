import UIKit

extension HierarchyByObsessionViewController {
    
    func applyDesign() {
        view.backgroundColor = ColorName.gray
        
        self.activityIndicator = self.getActivityIndicatorView()
        self.headerView = self.getHeaderView({ headerView in
            self.titleLabel = self.getTitleLabel(headerView: headerView, { titleLabel in
                self.pencylButton = self.getPencylButton(headerView: headerView, { pencylButton in
                    self.descriptionLabel = self.getDescriptionLabel(headerView: headerView, leadingView: titleLabel, trailingView: pencylButton)
                    self.collectionView = self.getCollectionView(topView: headerView, bottomView: self.view)
                })
            })
        })
    }
    
    private func getActivityIndicatorView(_ block: ((UIActivityIndicatorView) -> Void)? = nil) -> UIActivityIndicatorView {
        let activityIndicator = UIActivityIndicatorView(frame: view.frame)
        activityIndicator.backgroundColor = ColorName.turquoiseGreen
        
        block?(activityIndicator)
        return activityIndicator
    }
    
    private func getHeaderView(_ block: ((UIView) -> Void)? = nil) -> UIView {
        let headerView = UIView()
        headerView.backgroundColor = ColorName.turquoiseGreen
        
        view.addSubview(headerView, constraints: [
            constraint(\.topAnchor),
            constraint(\.leadingAnchor),
            constraint(\.trailingAnchor),
            constraint(\.heightAnchor, multiplier: 0.25),
        ])
        
        block?(headerView)
        return headerView
    }
    
    private func getTitleLabel(headerView: UIView, _ block: ((UILabel) -> Void)? = nil) -> UILabel {
        let label = UILabel()
        label.text = "Obsession"
        label.font = UIFont.boldSystemFont(ofSize: 22.0)
        label.textColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        
        headerView.addSubview(label, constraints: [
            constraint(\.safeAreaLayoutGuide.topAnchor),
            constraint(\.leadingAnchor, constant: 24.0)
        ])
        
        block?(label)
        return label
    }
    
    private func getDescriptionLabel(headerView: UIView, leadingView: UIView, trailingView: UIView, _ block: ((UILabel) -> Void)? = nil) -> UILabel {
        let label = UILabel()
        label.text = "How can I be sure this is the right person for me?"
        label.font = UIFont.systemFont(ofSize: 24.0)
        label.textColor = .white
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        
        headerView.addSubview(label)
        
        label.constrainToView(trailingView, constraints: [
            constraint(\.trailingAnchor, \.leadingAnchor, constraintRelation: .lessThanOrEqual, constant: -24.0)
        ])
        
        label.constrainToView(leadingView, constraints: [
            constraint(\.leadingAnchor),
            constraint(\.topAnchor, \.bottomAnchor, constant: 8.0)
        ])
        
        block?(label)
        return label
    }
    
    private func getPencylButton(headerView: UIView, _ block: ((UIButton) -> Void)? = nil) -> UIButton {
        let button = UIButton()
        button.setImage(Asset.edit.image, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        
        headerView.addSubview(button, constraints: [
            constraint(\.centerYAnchor, constant: 32.0),
            constraint(\.trailingAnchor, constant: -16.0)
        ])
        
        block?(button)
        return button
    }
    
    private func getCollectionView(topView: UIView, bottomView: UIView, _ block: ((UICollectionView) -> Void)? = nil) -> UICollectionView {
        let collectionViewLayout = UICollectionViewFlowLayout()
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewLayout)
        collectionView.tag = 999
        collectionView.backgroundColor = .clear
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        
        collectionView.register(HierarchyByObsessionCollectionViewCell.self, forCellWithReuseIdentifier: kHierarchyByObsessionIdentifier)
        
        view.addSubview(collectionView, constraints: [
            constraint(\.trailingAnchor),
            constraint(\.leadingAnchor),
        ])
        
        collectionView.constrainToView(topView, constraints: [
            constraint(\.topAnchor, \.bottomAnchor)
        ])
        
        collectionView.constrainToView(bottomView, constraints: [
            constraint(\.bottomAnchor)
        ])
        
        block?(collectionView)
        return collectionView
    }
    
}
