import UIKit
import NOCDKit

class HierarchyByObsessionViewController: BaseViewController<HierarchyByObsessionViewModel>, HierarchyByObsessionView {
    
    var headerView: UIView!
    var titleLabel: UILabel!
    var descriptionLabel: UILabel!
    var pencylButton: UIButton!
    var collectionView: UICollectionView!
    
    var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        applyDesign()
        
        // NavigationBar Clear
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
    }
    
    func update(with hierarchies: ResultGetHierarchyByObsessionAPIModel?) {
        guard let hierarchies = hierarchies else { return }
        self.descriptionLabel.text = hierarchies.obsession
        self.collectionView.reloadData()
    }
    
    func showLoader(text: String) {
        self.activityIndicator?.startAnimating()
    }
    
    func hideLoader() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.activityIndicator?.stopAnimating()
        }
    }
}

extension HierarchyByObsessionViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // Take Data Source By Collection View Tag ( 111, 112, 113 )
        let dataSources = viewModel.dataSource.compactMap { dataSource -> (HierarchyByObsessionDataSource?) in
            if dataSource.id == collectionView.tag { return dataSource } else { return nil }
        }
        
        if let dataSource = dataSources.first {
            return dataSource.data.count + 1
        } else {
            return viewModel.dataSource.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // Take Data Source By Collection View Tag ( 111, 112, 113 )
        let dataSources = viewModel.dataSource.compactMap { dataSource -> (HierarchyByObsessionDataSource?) in
            if dataSource.id == collectionView.tag {
                return dataSource
            } else {
                return nil
            }
        }
        
        guard let dataSource = dataSources.first else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kHierarchyByObsessionIdentifier, for: indexPath) as! HierarchyByObsessionCollectionViewCell
            let dataSource = viewModel.dataSource[indexPath.row]
            cell.configure(with: self, title: dataSource.title, description: dataSource.description)
            cell.collectionView.tag = 111 + indexPath.row
            return cell
        }
        
        // HierarchyByObsession Add Cell ( Last cell )
        if (indexPath.row == dataSource.data.count) || dataSource.data.isEmpty {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kHierarchyByObsessionAddIdentifier, for: indexPath) as! HierarchyByObsessionAddCollectionViewCell
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: dataSource.cellIdentifier, for: indexPath) as! HierarchyByObsessionTypeCollectionViewCellProtocol
            let data = dataSource.data[indexPath.row]
            cell.configure(with: data.title, header: data.header, color: data.color)
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        // Take Data Source By Collection View Tag ( 111, 112, 113 )
        let dataSources = viewModel.dataSource.compactMap { dataSource -> (HierarchyByObsessionDataSource?) in
            if dataSource.id == collectionView.tag { return dataSource } else { return nil }
        }
        
        if let dataSource = dataSources.first {
            let frame = self.view.frame
            var height: CGFloat = 72.0
            if indexPath.row == dataSource.data.count {
                return CGSize(width: frame.width, height: height)
            }
            if dataSource.cellIdentifier == kHierarchyByObsessionWithTitleAndPointIdentifier {
                height = height * 2
            }
            return CGSize(width: frame.width, height: height)
        } else {
            let frame = self.view.frame
            return CGSize(width: frame.width, height: 252.0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // Take Data Source By Collection View Tag ( 111, 112, 113 )
        let dataSources = viewModel.dataSource.compactMap { dataSource -> (HierarchyByObsessionDataSource?) in
            if dataSource.id == collectionView.tag { return dataSource } else { return nil }
        }
        
        if let dataSource = dataSources.first {
            if dataSource.cellIdentifier == kHierarchyByObsessionSimpleIdentifier {
//                dataSource.data[indexPath.row]
                guard let compulsionID = dataSource.compulsionID, let obsessionID = dataSource.obsessionID else { return }
                Coordinator.shared.goToEditCompulsionViewController(compulsionID: compulsionID, obsessionID: obsessionID)
            }
        }
    }
    
}
