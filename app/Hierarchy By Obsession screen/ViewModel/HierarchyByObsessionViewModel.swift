import UIKit
import NOCDKit

protocol HierarchyByObsessionView: BaseAsedoUI {
    func update(with hierarchies: ResultGetHierarchyByObsessionAPIModel?)
    func showLoader(text: String)
    func hideLoader()
}

struct HierarchyByObsessionDataSource {
    let id: Int
    var compulsionID: String?
    var obsessionID: String?
    let title: String
    let description: String
    let cellIdentifier: String
    var data: [HierarchyByObsessionDataSourceInformation]
}

struct HierarchyByObsessionDataSourceInformation {
    let color: UIColor?
    let title: String
    let header: String?
    let isCurrentERP: Bool?
}

class HierarchyByObsessionViewModel: ViewModel {
    
    let view: HierarchyByObsessionView
    let obsessionID: String
    let getHierarchiesByObsession: GetHierarchiesByObsession
    
    var dataSource: [HierarchyByObsessionDataSource] = [
        HierarchyByObsessionDataSource(id: 111,
                                       compulsionID: nil,
                                       obsessionID: nil,
                                       title: "Trigger",
                                       description: "People, things, and places that set off your obsession",
                                       cellIdentifier: kHierarchyByObsessionWithPointIdentifier,
                                       data: []),
        HierarchyByObsessionDataSource(id: 112,
                                       compulsionID: nil,
                                       obsessionID: nil,
                                       title: "Exposures",
                                       description: "Intentionally using triggers to face your obsession and get used to the distress",
                                       cellIdentifier: kHierarchyByObsessionWithTitleAndPointIdentifier,
                                       data: []),
        HierarchyByObsessionDataSource(id: 113,
                                       compulsionID: nil,
                                       obsessionID: nil,
                                       title: "Compulsions",
                                       description: "Ritualistic attempts to get rid of this distress",
                                       cellIdentifier: kHierarchyByObsessionSimpleIdentifier,
                                       data: [])
    ]
    
    var hierarchies: ResultGetHierarchyByObsessionAPIModel? {
        didSet {
            self.updateDataSource()
            self.view.update(with: hierarchies)
        }
    }
    
    init(view: HierarchyByObsessionView,
         obsessionID: String,
         getHierarchiesByObsession: GetHierarchiesByObsession) {
        self.view = view
        self.obsessionID = obsessionID
        self.getHierarchiesByObsession = getHierarchiesByObsession
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getHierarchiesByObsession(obsessionID: obsessionID)
    }
    
    private func getHierarchiesByObsession(obsessionID: String) {
        self.view.showLoader(text: "Getting hierarchies...")
        getHierarchiesByObsession.get(obsessionID: obsessionID) { hierarchiesAPIModel in
            self.view.hideLoader()
            if let hierarchies = hierarchiesAPIModel {
                self.hierarchies = hierarchies
            }
        }
    }
    
    private func updateDataSource() {
        if let triggers = hierarchies?.triggers {
            for trigger in triggers {
                // Trigger
                let data = HierarchyByObsessionDataSourceInformation(color: ColorName.circleRed,
                                                                     title: trigger.trigger,
                                                                     header: nil,
                                                                     isCurrentERP: nil)
                dataSource[0].data.append(data)
                
                // Exposures
                for exposure in trigger.exposures {
                    let exposureData = HierarchyByObsessionDataSourceInformation(color: ColorName.circleYellow,
                                                                                 title: exposure.exposure,
                                                                                 header: trigger.trigger,
                                                                                 isCurrentERP: exposure.currentERP)
                    dataSource[1].data.append(exposureData)
                }
            }
        }
        
        // Compulsions
        let compulsionData = HierarchyByObsessionDataSourceInformation(color: nil,
                                                                     title: "Asking other people if I’m in the right relationship",
                                                                     header: nil,
                                                                     isCurrentERP: nil)
        dataSource[2].data.append(compulsionData)
        
//            (dataSource[0].compulsionID,dataSource[1].compulsionID,dataSource[2].compulsionID) = (hierarchies?.compulsions.first?.compulsionID, hierarchies?.compulsions.first?.compulsionID, hierarchies?.compulsions.first?.compulsionID)
        (dataSource[0].compulsionID,dataSource[1].compulsionID,dataSource[2].compulsionID) = ("FCC89375-D881-4764-870B-802D8BD9FD64", "FCC89375-D881-4764-870B-802D8BD9FD64", "FCC89375-D881-4764-870B-802D8BD9FD64")
        (dataSource[0].obsessionID, dataSource[1].obsessionID, dataSource[2].obsessionID) = (hierarchies?.obsessionID, hierarchies?.obsessionID, hierarchies?.obsessionID)
    }
    
}
