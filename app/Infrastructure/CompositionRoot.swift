import UIKit
import NOCDKit

class CompositionRoot {
    static var kitCompositionRoot = NOCDKitCompositionRoot.sharedInstance
    static var sharedInstance: CompositionRoot = CompositionRoot()
    
    var rootTabBarController: UITabBarController!
    
    required init() {
        configureRootTabBarController()
    }
    
    private func configureRootTabBarController() {
        rootTabBarController = UITabBarController()
        //        rootTabBarController.tabBar.barTintColor = Theme.current.mainColor
        rootTabBarController.tabBar.isTranslucent = false
        
//        var viewControllersList: [UIViewController] = [UIViewController]()
        
        //        let mainFeedViewController = resolveMainFeedViewController()
        //        mainFeedViewController.tabBarItem = UITabBarItem(title: "", image: Asset.homeTabIcon.image, tag: 0)
        //        mainFeedViewController.tabBarItem.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        //        viewControllersList.append(mainFeedViewController)
        //
        //        let exampleViewController3 = resolveMainFeedViewController()
        //        exampleViewController3.tabBarItem = UITabBarItem(title: "", image: Asset.searchTabIcon.image, tag: 1)
        //        exampleViewController3.tabBarItem.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        //        viewControllersList.append(exampleViewController3)
        //
        //        let addGoalViewController = resolveAddGoalViewController()
        //        addGoalViewController.tabBarItem = UITabBarItem(title: "", image: Asset.pushTabIcon.image, tag: 2)
        //        addGoalViewController.tabBarItem.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        //        viewControllersList.append(addGoalViewController)
        //
        //        let exampleViewController1 = resolveTopicViewController()
        //        exampleViewController1.tabBarItem = UITabBarItem(title: "", image: Asset.historyTabIcon.image, tag: 3)
        //        exampleViewController1.tabBarItem.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        //        viewControllersList.append(exampleViewController1)
        
//        let loginViewController = resolveLoginViewController()
//        //        loginViewController.tabBarItem = UITabBarItem(title: "", image: Asset.personTabIcon.image, tag: 4)
//        loginViewController.tabBarItem.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
//        viewControllersList.append(loginViewController)
//
//        let viewControllers = viewControllersList.map { (viewController) -> UIViewController in
//            let navigationController = UINavigationController(rootViewController: viewController)
//            //            navigationController.navigationBar.barTintColor = Theme.current.mainColor
//            navigationController.navigationBar.isTranslucent = false
//            return navigationController
//        }
//
//        rootTabBarController.setViewControllers(viewControllers, animated: true)
    }
    
    // MARK: ViewControllers
    
    func resolveHierarchiesViewController() -> HierarchiesViewController {
        let vc = HierarchiesViewController()
        vc.viewModel = resolveHierarchiesViewModel(view: vc)
        return vc
    }
    
    func resolveSignInViewController() -> SignInViewController {
        let vc = SignInViewController()
        vc.viewModel = resolveSignInViewModel(view: vc)
        return vc
    }
    
    func resolveAddHierarchiesViewController() -> AddHierarchiesViewController {
        let vc = AddHierarchiesViewController()
        vc.viewModel = resolveAddHierarchiesViewModel(view: vc)
        return vc
    }

    func resolveHierarchyByObsessionViewController(obsessionID: String) -> HierarchyByObsessionViewController {
        let vc = HierarchyByObsessionViewController()
        vc.viewModel = resolveHierarchyByObsessionViewModel(view: vc, obsessionID: obsessionID)
        return vc
    }
    
    func resolveEditCompulsionViewController(compulsionID: String, obsessionID: String) -> EditCompulsionViewController {
        let vc = EditCompulsionViewController()
        vc.viewModel = resolveEditCompulsionViewModel(view: vc, obsessionID: obsessionID, compulsionID: compulsionID)
        return vc
    }
    
    // MARK: - View Model
    
    func resolveHierarchiesViewModel(view: HierarchiesView) -> HierarchiesViewModel {
        return HierarchiesViewModel(view: view,
                                    getHierarchies: CompositionRoot.kitCompositionRoot.resolveGetHierarchies)
    }
    
    func resolveSignInViewModel(view: SignInView) -> SignInViewModel {
        return SignInViewModel(view: view,
                               authLogin: CompositionRoot.kitCompositionRoot.resolveAuthLogin)
    }
    
    func resolveAddHierarchiesViewModel(view: AddHierarchiesView) -> AddHierarchiesViewModel {
        return AddHierarchiesViewModel(view: view, dataSource: CompositionRoot.kitCompositionRoot.resolveAddHierarchiesTemplatesDataSource)
    }
    
    func resolveHierarchyByObsessionViewModel(view: HierarchyByObsessionView, obsessionID: String) -> HierarchyByObsessionViewModel {
        return HierarchyByObsessionViewModel(view: view,
                                             obsessionID: obsessionID,
                                             getHierarchiesByObsession: CompositionRoot.kitCompositionRoot.resolveGetHierarchiesByObsession)
    }
    
    func resolveEditCompulsionViewModel(view: EditCompulsionView, obsessionID: String, compulsionID: String) -> EditCompulsionViewModel {
        return EditCompulsionViewModel(view: view, obsessionID: obsessionID, compulsionID: compulsionID, delete: CompositionRoot.kitCompositionRoot.resolveDeleteCompulsion, edit: CompositionRoot.kitCompositionRoot.resolveEditCompulsion)
    }
}
