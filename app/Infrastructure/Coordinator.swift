import UIKit

class Coordinator {
    
    static let shared = Coordinator()
    
    private var compositionRoot: CompositionRoot {
        return CompositionRoot.sharedInstance
    }
    
    private var baseNavigationController: UINavigationController? {
        get {
            if let navigationController = lastPresentedViewController?.navigationController {
                return navigationController
            } else {
                return appWindow?.rootViewController as? UINavigationController
            }
        }
    }
    
    private var appDelegate: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    private var lastPresentedViewController: UIViewController?
    
    private init() { }
    
    func goToHierarchiesViewController() {
        present(compositionRoot.resolveHierarchiesViewController())
    }
    
    func goToAddHierarchiesViewController() {
        present(compositionRoot.resolveAddHierarchiesViewController())
    }
    
    func goToHierarchyByObsessionViewController(obsessionID: String) {
        present(compositionRoot.resolveHierarchyByObsessionViewController(obsessionID: obsessionID))
    }
    
    func goToEditCompulsionViewController(compulsionID: String, obsessionID: String) {
        present(compositionRoot.resolveEditCompulsionViewController(compulsionID: compulsionID, obsessionID: obsessionID))
    }
    
    func push(_ vc: UIViewController) {
        if let baseNavigationController = baseNavigationController {
            baseNavigationController.pushViewController(vc, animated: true)
            lastPresentedViewController = vc
        }
    }
    
    func present(_ vc: UIViewController) {
        if let baseNavigationController = baseNavigationController {
            baseNavigationController.pushViewController(vc, animated: true)
            lastPresentedViewController = vc
        }
    }
    
    func setNavigationsetViewController(_ vc: UIViewController) {
        if let baseNavigationController = baseNavigationController {
            baseNavigationController.setViewControllers([vc], animated: true)
            lastPresentedViewController = vc
        }
    }
    
}
