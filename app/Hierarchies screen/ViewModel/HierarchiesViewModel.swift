import Foundation
import NOCDKit

protocol HierarchiesView {
    func update(with hierarchies: ResultGetHierarchiesAPIModel?)
    func showLoader(text: String)
    func hideLoader()
}

class HierarchiesViewModel: ViewModel {
    
    let view: HierarchiesView
    let getHierarchies: GetHierarchies
    
    var hierarchies: ResultGetHierarchiesAPIModel? {
        didSet {
            self.view.update(with: hierarchies)
        }
    }
    
    init(view: HierarchiesView, getHierarchies: GetHierarchies) {
        self.view = view
        self.getHierarchies = getHierarchies
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getHierarchiesRequest()
    }
    
    private func getHierarchiesRequest() {
        self.view.showLoader(text: "Getting hierarchies...")
        getHierarchies.get { hierarchiesAPIModel in
            self.view.hideLoader()
            if let hierarchies = hierarchiesAPIModel {
                self.hierarchies = hierarchies
            }
        }
    }
    
}
