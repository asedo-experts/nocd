import UIKit
import NOCDKit

class HierarchiesViewController: BaseViewController<HierarchiesViewModel>, HierarchiesView {

    let kObsessionIdentifier = "obsessionCell"
    
    var titleLabel: UILabel!
    var descriptionLabel: UILabel!
    var hierarchiesCollectionView: UICollectionView!
    var addHierachyButton: UIButton!
    var loadingView: LoadingAnimationView!
    var activityIndicator: UIActivityIndicatorView!
    
    override func loadView() {
        super.loadView()
        applyDesign()
    }
    
    func update(with hierarchies: ResultGetHierarchiesAPIModel?) {
        hierarchiesCollectionView.reloadData()
    }
    
    func showLoader(text: String) {
//        loadingView.showLoader(text: text)
        activityIndicator.startAnimating()
    }
    
    func hideLoader() {
//        loadingView.hideLoader()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.activityIndicator.stopAnimating()
        }
    }
    
    @objc func addHierachyButtonPressed() {
        Coordinator.shared.goToAddHierarchiesViewController()
    }
}

extension HierarchiesViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.hierarchies?.obsessions?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kObsessionIdentifier, for: indexPath) as! HierarchiesViewObsessionCollectionViewCell
        let obsessions = viewModel.hierarchies?.obsessions?[indexPath.row]
        cell.configure(with: obsessions?.obsession ?? "Default Obsession")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.bounds.size.width-16.0)/2
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let obsession = viewModel.hierarchies?.obsessions?[indexPath.row] {
            Coordinator.shared.goToHierarchyByObsessionViewController(obsessionID: obsession.obsessionID)
        }
    }
}
