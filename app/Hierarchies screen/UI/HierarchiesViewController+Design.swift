import UIKit

extension HierarchiesViewController {
    
    func applyDesign() {
        view.backgroundColor = .white
        titleLabel = getTitleLabel { titleLabel in
            self.descriptionLabel = self.getDescriptionLabel(leadingView: titleLabel, { descriptionLabel in
                self.addHierachyButton = self.getAddHierachyButton { addHierachyButton in
                    self.hierarchiesCollectionView = self.getHierarchiesCollectionView(topView: descriptionLabel, bottomView: addHierachyButton)
                }
            })
        }
        loadingView = getLoadingView()
        activityIndicator = getActivityIndicatorView()
    }
    
    private func getTitleLabel(_ block: ((UILabel) -> Void)? = nil) -> UILabel {
        let label = UILabel()
        label.text = "My hierarchies"
        label.font = UIFont.boldSystemFont(ofSize: 22.0)
        label.textColor = ColorName.azure
        label.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(label, constraints: [
            constraint(\.safeAreaLayoutGuide.topAnchor, constant: 32.0),
            constraint(\.leadingAnchor, constant: 24.0)
        ])
        
        block?(label)
        return label
    }
    
    private func getDescriptionLabel(leadingView: UIView, _ block: ((UILabel) -> Void)? = nil) -> UILabel {
        let label = UILabel()
        label.text = "These are symptom lists, grouped by obsession and ranked by difficulty."
        label.font = UIFont.systemFont(ofSize: 16.0)
        label.textColor = ColorName.azure
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(label, constraints: [
            constraint(\.trailingAnchor, constraintRelation: .lessThanOrEqual, constant: 16.0)
        ])
        
        label.constrainToView(leadingView, constraints: [
            constraint(\.leadingAnchor),
            constraint(\.topAnchor, \.bottomAnchor, constant: 8.0)
        ])
        block?(label)
        return label
    }
    
    private func getAddHierachyButton(_ block: ((UIButton) -> Void)? = nil) -> UIButton {
        let button = UIButton()
        button.cornerRadius(radius: 6.0)
        button.setTitle("Add a hierachy", for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18.0)
        button.backgroundColor = ColorName.bondiBlue
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(addHierachyButtonPressed), for: .touchUpInside)
        
        view.addSubview(button, constraints: [
            constraint(\.centerXAnchor),
            constraint(\.heightAnchor, multiplier: 5/100),
            constraint(\.safeAreaLayoutGuide.bottomAnchor, constraintRelation: .lessThanOrEqual),
            constraint(\.widthAnchor, multiplier: 0.65)
        ])
        
        block?(button)
        return button
    }
    
    private func getHierarchiesCollectionView(topView: UIView, bottomView: UIView, _ block: ((UICollectionView) -> Void)? = nil) -> UICollectionView {
        let collectionViewLayout = UICollectionViewFlowLayout()
        let collecionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewLayout)
        collecionView.backgroundColor = .clear
        collecionView.delegate = self
        collecionView.dataSource = self
        collecionView.translatesAutoresizingMaskIntoConstraints = false
        
        collecionView.register(HierarchiesViewObsessionCollectionViewCell.self, forCellWithReuseIdentifier: kObsessionIdentifier)
        
        view.addSubview(collecionView, constraints: [
            constraint(\.trailingAnchor, constant: -24.0),
            constraint(\.leadingAnchor, constant: 24.0),
            constraint(\.heightAnchor, multiplier: 25/100)
        ])
        
        collecionView.constrainToView(topView, constraints: [
            constraint(\.topAnchor, \.bottomAnchor, constant: 24.0)
        ])
        
        collecionView.constrainToView(bottomView, constraints: [
            constraint(\.bottomAnchor, \.topAnchor, constant: -16.0)
        ])
        
        block?(collecionView)
        return collecionView
    }
    
    private func getActivityIndicatorView(_ block: ((UIActivityIndicatorView) -> Void)? = nil) -> UIActivityIndicatorView {
        let activityIndicatorView = UIActivityIndicatorView()
        activityIndicatorView.hidesWhenStopped = true
        
        let barButtonItem = UIBarButtonItem(customView: activityIndicatorView)
        navigationItem.setRightBarButton(barButtonItem, animated: true)
        
        block?(activityIndicatorView)
        return activityIndicatorView
    }
    
    private func getLoadingView(_ block: ((LoadingAnimationView) -> Void)? = nil) -> LoadingAnimationView {
        let loadingView = LoadingAnimationView()
        loadingView.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(loadingView, constraints: [
            constraint(\.centerXAnchor),
            constraint(\.centerYAnchor),
            constraint(\.heightAnchor, multiplier: 12/100),
            constraint(\.widthAnchor, \.heightAnchor, multiplier: 12/100),
        ])
        
        view.bringSubviewToFront(loadingView)
        block?(loadingView)
        return loadingView
    }
    
    
}
