import UIKit

class HierarchiesViewObsessionCollectionViewCell: UICollectionViewCell {
    
    var titleLabel: UILabel!
    var descriptionLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        applyDesign()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func configure(with obsession: String) {
        descriptionLabel.text = obsession
    }
    
    func applyDesign() {
        backgroundColor = ColorName.turquoiseGreen
        cornerRadius(radius: 3.0)
        titleLabel = getTitleLabel({ titleLabel in
            self.descriptionLabel = self.getDescriptionLabel(leadingView: titleLabel)
        })
        layoutIfNeeded()
    }
    
    private func getTitleLabel(_ block: ((UILabel) -> Void)? = nil) -> UILabel {
        let label = UILabel()
        label.text = "OBSESSION"
        label.font = UIFont.boldSystemFont(ofSize: 13.0)
        label.textColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(label, constraints: [
            constraint(\.topAnchor, constant: 24.0),
            constraint(\.leadingAnchor, constant: 24.0)
        ])
        
        block?(label)
        return label
    }
    
    private func getDescriptionLabel(leadingView: UIView, _ block: ((UILabel) -> Void)? = nil) -> UILabel {
        let label = UILabel()
        label.text = "How can I be sure this is the right person for me?"
        label.font = UIFont.boldSystemFont(ofSize: 18.0)
        label.minimumScaleFactor = 0.5
        label.adjustsFontSizeToFitWidth = true
        label.textColor = .white
        label.contentMode = .center
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(label, constraints: [
            constraint(\.trailingAnchor, constraintRelation: .lessThanOrEqual, constant: -24.0),
            constraint(\.bottomAnchor, constraintRelation: .lessThanOrEqual, constant: -24.0)
        ])
        
        label.constrainToView(leadingView, constraints: [
            constraint(\.leadingAnchor),
            constraint(\.topAnchor, \.bottomAnchor, constant: 16.0)
        ])
        
        block?(label)
        return label
    }
    
}
