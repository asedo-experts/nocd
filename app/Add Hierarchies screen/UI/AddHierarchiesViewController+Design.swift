import UIKit

extension AddHierarchiesViewController {
    
    func applyDesign() {
        view.backgroundColor = .white
        scrollView = getScrollView({ scrollView in
            self.backView = self.getBackView(scrollView: scrollView, { view in
                self.titleLabel = self.getTitleLabel(backView: view, { titleLabel in
                    self.descriptionLabel = self.getDescriptionLabel(backView: view, leadingView: titleLabel, { descriptionLabel in
                        self.addMyOwnButton = self.getAddMyOwnButton(backView: view, leadingView: descriptionLabel, { addMyOwnButton in
                            self.hierarchiesCollectionViewHeaderLabel = self.getHierarchiesCollectionViewHeaderLabel(backView: view, leadingView: addMyOwnButton, { collectionViewHeaderLabel in
                                self.addMyOwnFooterButton = self.getAddMyOwnFooterButton(backView: view) { addMyOwnFooterButton in
                                    self.hierarchiesCollectionView = self.getHierarchiesCollectionView(backView: view,
                                                                                                       topView: collectionViewHeaderLabel,
                                                                                                       bottomView: addMyOwnFooterButton)
                                }
                            })
                        })
                    })
                })
            })
        })
        loadingView = getLoadingView()
        activityIndicator = getActivityIndicatorView()
    }
    
    private func getScrollView(_ block: ((UIScrollView) -> Void)? = nil) -> UIScrollView {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(scrollView, constraints: [
            constraint(\.topAnchor),
            constraint(\.bottomAnchor),
            constraint(\.leadingAnchor),
            constraint(\.trailingAnchor),
        ])
        
        block?(scrollView)
        return scrollView
    }
    
    private func getBackView(scrollView: UIScrollView, _ block: ((UIView) -> Void)? = nil) -> UIView {
        let backView = UIView()
        backView.translatesAutoresizingMaskIntoConstraints = false
        
        backViewHeightConstaint = NSLayoutConstraint(item: backView,
                                                       attribute: .height,
                                                       relatedBy: .equal,
                                                       toItem: nil,
                                                       attribute: .notAnAttribute,
                                                       multiplier: 1.0,
                                                       constant: 400.0)
        
        backView.addConstraint(backViewHeightConstaint)
        
        scrollView.addSubview(backView, constraints: [
            constraint(\.topAnchor),
            constraint(\.bottomAnchor),
            constraint(\.leadingAnchor),
            constraint(\.trailingAnchor),
            constraint(\.widthAnchor),
            constraint(\.heightAnchor)
        ])
        
        block?(backView)
        return backView
    }
    
    private func getTitleLabel(backView: UIView, _ block: ((UILabel) -> Void)? = nil) -> UILabel {
        let label = UILabel()
        label.text = "What is bothering you?"
        label.font = UIFont.boldSystemFont(ofSize: 22.0)
        label.textColor = ColorName.azure
        label.translatesAutoresizingMaskIntoConstraints = false
        
        backView.addSubview(label, constraints: [
            constraint(\.safeAreaLayoutGuide.topAnchor, constant: 32.0),
            constraint(\.leadingAnchor, constant: 24.0)
        ])
        
        block?(label)
        return label
    }
    
    private func getDescriptionLabel(backView: UIView, leadingView: UIView, _ block: ((UILabel) -> Void)? = nil) -> UILabel {
        let label = UILabel()
        label.text = "Do you have any repetitive and unwanted images, thoughts, or urges?"
        label.font = UIFont.systemFont(ofSize: 16.0)
        label.textColor = ColorName.azure
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        
        backView.addSubview(label, constraints: [
            constraint(\.trailingAnchor, constraintRelation: .lessThanOrEqual, constant: -24.0)
        ])
        
        label.constrainToView(leadingView, constraints: [
            constraint(\.leadingAnchor),
            constraint(\.topAnchor, \.bottomAnchor, constant: 8.0)
        ])
        block?(label)
        return label
    }
    
    private func getAddMyOwnButton(backView: UIView, leadingView: UIView, _ block: ((UIButton) -> Void)? = nil) -> UIButton {
        let button = UIButton()
        button.cornerRadius(radius: 17.0)
        button.setTitle("Add my own", for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 17.0)
        button.backgroundColor = ColorName.bondiBlue
        button.translatesAutoresizingMaskIntoConstraints = false
        
        backView.addSubview(button, constraints: [
            constraint(\.widthAnchor, multiplier: 40/100)
        ])
        
        button.constrainToView(leadingView, constraints: [
            constraint(\.leadingAnchor),
            constraint(\.topAnchor, \.bottomAnchor, constant: 24.0),
        ])
        
        block?(button)
        return button
    }
    
    private func getHierarchiesCollectionViewHeaderLabel(backView: UIView, leadingView: UIView, _ block: ((UILabel) -> Void)? = nil) -> UILabel {
        let label = UILabel()
        label.text = "Or pick one from these. You will be able to change or add more later."
        label.font = UIFont.systemFont(ofSize: 14.0)
        label.textColor = ColorName.azure
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        
        backView.addSubview(label, constraints: [
            constraint(\.trailingAnchor, constraintRelation: .lessThanOrEqual, constant: 24.0)
        ])
        
        label.constrainToView(leadingView, constraints: [
            constraint(\.leadingAnchor),
            constraint(\.topAnchor, \.bottomAnchor, constant: 24.0)
        ])
        
        block?(label)
        return label
    }
    
    private func getAddMyOwnFooterButton(backView: UIView, _ block: ((UIButton) -> Void)? = nil) -> UIButton {
        let button = UIButton()
        button.cornerRadius(radius: 6.0)
        button.setTitle("Add my own", for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18.0)
        button.backgroundColor = ColorName.bondiBlue
        button.translatesAutoresizingMaskIntoConstraints = false
        
        backView.addSubview(button, constraints: [
            constraint(\.centerXAnchor),
            constraint(\.widthAnchor, multiplier: 65/100),
            constraint(\.safeAreaLayoutGuide.bottomAnchor, constant: -16.0),
        ])
        
        block?(button)
        return button
    }
    
    private func getHierarchiesCollectionView(backView: UIView, topView: UIView, bottomView: UIView, _ block: ((UICollectionView) -> Void)? = nil) -> UICollectionView {
        let collectionViewLayout = UICollectionViewFlowLayout()
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewLayout)
        collectionView.backgroundColor = .clear
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        
        collectionView.register(AddHierarchiesViewCollectionViewCell.self, forCellWithReuseIdentifier: kHierarchyIdentifier)
        
        hierarchiesCollectionViewHeightConstaint = NSLayoutConstraint(item: collectionView,
                                                                attribute: .height,
                                                                relatedBy: .equal,
                                                                toItem: nil,
                                                                attribute: .notAnAttribute,
                                                                multiplier: 1.0,
                                                                constant: 80.0)
        
//        collectionView.addConstraint(hierarchiesCollectionViewHeightConstaint)
        
        backView.addSubview(collectionView, constraints: [
            constraint(\.trailingAnchor),
            constraint(\.leadingAnchor),
        ])
        
        collectionView.constrainToView(topView, constraints: [
            constraint(\.topAnchor, \.bottomAnchor, constant: 24.0)
        ])
        
        collectionView.constrainToView(bottomView, constraints: [
            constraint(\.bottomAnchor, \.topAnchor, constant: -16.0)
        ])
        
        block?(collectionView)
        return collectionView
    }
    
    private func getActivityIndicatorView(_ block: ((UIActivityIndicatorView) -> Void)? = nil) -> UIActivityIndicatorView {
        let activityIndicatorView = UIActivityIndicatorView()
        activityIndicatorView.hidesWhenStopped = true
        
        let barButtonItem = UIBarButtonItem(customView: activityIndicatorView)
        navigationItem.setRightBarButton(barButtonItem, animated: true)
        
        block?(activityIndicatorView)
        return activityIndicatorView
    }
    
    private func getLoadingView(_ block: ((LoadingAnimationView) -> Void)? = nil) -> LoadingAnimationView {
        let loadingView = LoadingAnimationView()
        loadingView.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(loadingView, constraints: [
            constraint(\.centerXAnchor),
            constraint(\.centerYAnchor),
            constraint(\.heightAnchor, multiplier: 12/100),
            constraint(\.widthAnchor, \.heightAnchor, multiplier: 12/100),
        ])
        
        view.bringSubviewToFront(loadingView)
        block?(loadingView)
        return loadingView
    }

}
