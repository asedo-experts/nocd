import UIKit

class AddHierarchiesViewCollectionViewCell: UICollectionViewCell {
    
    var headLabel: UILabel!
    var backView: UIView!
    var imageView: UIImageView!
    var titleLabel: UILabel!
    var quoteImageView: UIView!
    var descriptionLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        applyDesign()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func configure(with head: String, title: String, description: String, image: UIImage) {
        headLabel.text = head
        titleLabel.text = title
        descriptionLabel.text = description
        imageView.image = image
    }
    
    func applyDesign() {
        backgroundColor = .white
        headLabel = getHeaderLabel({ titleLabel in
            self.backView = self.getBackView(leadingView: titleLabel, { backView in
                self.imageView = self.getImageView(backView: backView, { imageView in
                    self.titleLabel = self.getTitleLabel(backView: backView, imageView: imageView, { titleView in
                        self.descriptionLabel = self.getDescriptionLabel(backView: backView, leadingView: titleView)
                        self.quoteImageView = self.getQuoteImageView(backView: backView, centeredView: titleView)
                    })
                })
            })
        })
        layoutIfNeeded()
    }
    
    private func getHeaderLabel(_ block: ((UILabel) -> Void)? = nil) -> UILabel {
        let label = UILabel()
        label.text = "HARM OCD"
        label.font = UIFont.boldSystemFont(ofSize: 14.0)
        label.textColor = ColorName.azure
        label.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(label, constraints: [
            constraint(\.topAnchor, constant: 8.0),
            constraint(\.leadingAnchor, constant: 24.0),
            constraint(\.heightAnchor, multiplier: 6/100)
        ])
                block?(label)
        return label
    }
    
    private func getBackView(leadingView: UIView, _ block: ((UIView) -> Void)? = nil) -> UIView {
        let backView = UIView()
        backView.backgroundColor = .white
        backView.cornerRadius(radius: 6.0)
        backView.border(color: .lightGray)
        backView.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(backView, constraints: [
            constraint(\.trailingAnchor, constant: -24.0),
            constraint(\.bottomAnchor, constant: -8.0)
        ])
        
        backView.constrainToView(leadingView, constraints: [
            constraint(\.topAnchor, \.bottomAnchor, constant: 8.0),
            constraint(\.leadingAnchor)
        ])
        
        block?(backView)
        return backView
    }
    
    private func getImageView(backView: UIView, _ block: ((UIImageView) -> Void)? = nil) -> UIImageView {
        let imageView = UIImageView()
        imageView.image = Asset.harmOcd.image
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        backView.addSubview(imageView, constraints: [
            constraint(\.centerYAnchor),
            constraint(\.heightAnchor, multiplier: 40/100),
            constraint(\.widthAnchor, \.heightAnchor, multiplier: 40/100),
            constraint(\.leadingAnchor, constant: 32.0)
        ])
        
        block?(imageView)
        return imageView
    }
    
    private func getTitleLabel(backView: UIView, imageView: UIView, _ block: ((UILabel) -> Void)? = nil) -> UILabel {
        let label = UILabel()
        label.text = "I could hurt myself or others, or bad things will happen to me."
        label.font = UIFont.systemFont(ofSize: 20.0)
        label.numberOfLines = 0
        label.textColor = ColorName.black
        label.translatesAutoresizingMaskIntoConstraints = false
        
        backView.addSubview(label, constraints: [
            constraint(\.topAnchor, constant: 24.0),
            constraint(\.trailingAnchor, constraintRelation: .lessThanOrEqual, constant: -24.0)
        ])
        
        label.constrainToView(imageView, constraints: [
            constraint(\.leadingAnchor, \.trailingAnchor, constant: 32.0),
        ])
        
        block?(label)
        return label
    }
    
    private func getDescriptionLabel(backView: UIView, leadingView: UIView, _ block: ((UILabel) -> Void)? = nil) -> UILabel {
        let label = UILabel()
        label.text = "Intrusive thoughts about hurting others, including loved one."
        label.font = UIFont.systemFont(ofSize: 16.0)
        label.numberOfLines = 0
        label.textColor = ColorName.black
        label.alpha = 0.68
        label.translatesAutoresizingMaskIntoConstraints = false
        
        backView.addSubview(label, constraints: [
//            constraint(\.bottomAnchor, constant: -24.0)
        ])
        
        backView.addSubview(label, constraints: [
            constraint(\.trailingAnchor, constraintRelation: .lessThanOrEqual, constant: -24.0)
        ])
        
        label.constrainToView(leadingView, constraints: [
            constraint(\.topAnchor, \.bottomAnchor, constant: 4.0),
            constraint(\.leadingAnchor)
        ])
        
        block?(label)
        return label
    }
    
    private func getQuoteImageView(backView: UIView, centeredView: UIView, _ block: ((UIImageView) -> Void)? = nil) -> UIImageView {
        let imageView = UIImageView()
        imageView.image = Asset.quote.image
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        backView.addSubview(imageView)
        
        imageView.addSubview(centeredView, constraints: [
            constraint(\.leadingAnchor),
            constraint(\.topAnchor, constant: 7.0)
        ])
        
        block?(imageView)
        return imageView
    }
    
}
