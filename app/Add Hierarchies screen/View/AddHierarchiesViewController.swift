import UIKit

class AddHierarchiesViewController: BaseViewController<AddHierarchiesViewModel>, AddHierarchiesView {
    
    let kHierarchyIdentifier = "hierarchyCell"
    
    var scrollView: UIScrollView!
    var backView: UIView!
    var backViewHeightConstaint: NSLayoutConstraint!
    
    var titleLabel: UILabel!
    var descriptionLabel: UILabel!
    var addMyOwnButton: UIButton!
    
    var hierarchiesCollectionViewHeaderLabel: UILabel!
    var hierarchiesCollectionView: UICollectionView!
    var hierarchiesCollectionViewHeightConstaint: NSLayoutConstraint!
    var addMyOwnFooterButton: UIButton!
    
    var loadingView: LoadingAnimationView!
    var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        applyDesign()
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
//            self.hierarchiesCollectionViewHeightConstaint.constant = self.hierarchiesCollectionView.contentSize.height
//            self.backViewHeightConstaint.constant = self.backView.frame.height + self.hierarchiesCollectionView.contentSize.height
//            self.view.layoutIfNeeded()
//        }
    }
    
}

extension AddHierarchiesViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.dataSource.dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kHierarchyIdentifier, for: indexPath) as! AddHierarchiesViewCollectionViewCell
        let data = viewModel.data(index: indexPath.row)
        let image = UIImage(named: data.image) ?? Asset.harmOcd.image
        cell.configure(with: data.head, title: data.title, description: data.description, image: image)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let data = viewModel.data(index: indexPath.row)
        
        let width = collectionView.bounds.size.width
        let titleHeight = data.title.height(withConstrainedWidth: width/2.5,
                                            font: .systemFont(ofSize: 18.0))
        let descriptionHeight = data.description.height(withConstrainedWidth: width/2.5,
                                                        font: .systemFont(ofSize: 14.0)) + 8
        return CGSize(width: width, height: titleHeight + descriptionHeight + 24*2 + 32)
    }
}
