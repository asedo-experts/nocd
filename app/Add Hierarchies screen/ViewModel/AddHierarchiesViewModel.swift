import Foundation
import NOCDKit

protocol AddHierarchiesView {
    
}

class AddHierarchiesViewModel: ViewModel {
    
    let view: AddHierarchiesView
    let dataSource: AddHierarchiesTemplatesDataSource
    
    init(view: AddHierarchiesView, dataSource: AddHierarchiesTemplatesDataSource) {
        self.view = view
        self.dataSource = dataSource
    }
    
    public func data(index: Int) -> AddHierarchiesTemplateModel {
        return dataSource.dataSource[index]
    }
    
}
