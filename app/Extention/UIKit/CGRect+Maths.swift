import UIKit

extension CGRect {
    public func getPercentage(_ value: CGFloat) -> CGRect {
        let newWidth = width * (value / 100.0)
        let newHeight = height * (value / 100.0)

        return CGRect(origin: CGPoint(x: (width - newWidth) / 2, y: (height - newHeight) / 2),
                      size: CGSize(width: newWidth, height: newHeight))
    }

    public func getOriginOfCentering(_ rect: CGRect) -> CGPoint {
        return CGPoint(x: (width - rect.width) / 2,
                       y: (height - rect.height) / 2)
    }
}
