import UIKit

extension CGSize {
    func percentage(_ value: CGFloat) -> CGSize {
        return CGSize(width: width * (value / 100),
                      height: height * (value / 100))
    }

    func resize(maxSideLengthInPx: CGFloat) -> CGSize {
        let finalWidth: CGFloat
        let finalHeight: CGFloat

        if width < height {
            finalHeight = min(maxSideLengthInPx, height)
            finalWidth = width * (finalHeight / height)
        } else {
            finalWidth = min(maxSideLengthInPx, width)
            finalHeight = height * (finalWidth / width)
        }

        return CGSize(width: finalWidth, height: finalHeight)
    }

    var aspectRatio: CGFloat {
        return width / height
    }
}
