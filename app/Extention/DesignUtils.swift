import UIKit

class DesignUtils {
    class func initScrollViewAutolayout(inViewController: UIViewController,
                                        below belowView: UIView? = nil,
                                        above aboveView: UIView? = nil) -> UIScrollView {
        let scrollView = UIScrollView()
        if #available(iOS 11, *) {
            scrollView.contentInsetAdjustmentBehavior = UIScrollView.ContentInsetAdjustmentBehavior.never
        } else {
            inViewController.automaticallyAdjustsScrollViewInsets = false
        }
        scrollView.contentInset = .zero
        scrollView.scrollIndicatorInsets = .zero
        scrollView.contentOffset = CGPoint(x: 0.0, y: 0.0)
        inViewController.view.addSubview(scrollView, constraints: [
            constraint(\.leadingAnchor),
            constraint(\.trailingAnchor),
        ])

        if let belowView = belowView {
            scrollView.constrainToView(belowView, constraints: [
                DesignUtils.verticalSpacingConstraint(0),
            ])
        } else {
            scrollView.constrainToView(inViewController.view, constraints: [
                constraint(\.topAnchor),
            ])
        }

        if let aboveView = aboveView {
            aboveView.constrainToView(scrollView, constraints: [
                DesignUtils.verticalSpacingConstraint(0),
            ])
        } else {
            scrollView.constrainToView(inViewController.view, constraints: [
                constraint(\.bottomAnchor),
            ])
        }
        return scrollView
    }

    class func initScrollableView(scrollView: UIScrollView) -> UIView {
        let scrollableContentView = UIView()
        scrollView.addSubview(scrollableContentView, constraints: [
            constraint(\.topAnchor),
            constraint(\.leadingAnchor),
            constraint(\.bottomAnchor),
            constraint(\.trailingAnchor),
            constraint(\.centerXAnchor),
            constraint(\.centerYAnchor),
        ])
        return scrollableContentView
    }

    class func match() -> [PairedConstraint] {
        return [
            constraint(\.topAnchor),
            constraint(\.bottomAnchor),
            constraint(\.leadingAnchor),
            constraint(\.trailingAnchor),
        ]
    }

    class func verticalSpacingConstraint(_ offset: CGFloat) -> PairedConstraint {
        return constraint(\.topAnchor, \.bottomAnchor, constant: offset)
    }

    class func horizontalSpacingConstraint(_ offset: CGFloat) -> PairedConstraint {
        return constraint(\.leadingAnchor, \.trailingAnchor, constant: offset)
    }

    class func atLeft(_ offset: CGFloat = 0) -> PairedConstraint {
        return constraint(\.trailingAnchor, \.leadingAnchor, constant: -offset)
    }
}
