import UIKit

extension EditCompulsionViewController {
    
    func applyDesign() {
        view.backgroundColor = .white
        
        self.deleteButton = self.getDeleteButton()
        self.doneButton = self.getDoneButton()
        self.textView = self.getTextView()
        self.alertController = self.alertController(deleteAction: deleteAction, cancelAction: cancelAction)
        
        let deleteBarButtonItem = UIBarButtonItem(customView: deleteButton)
        let spaceBarButtonItem = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        spaceBarButtonItem.width = 16
        let doneBarButtonItem = UIBarButtonItem(customView: doneButton)
        
        self.navigationItem.rightBarButtonItems = [doneBarButtonItem, spaceBarButtonItem, deleteBarButtonItem]
    }
    
    private func getDeleteButton(_ block: ((UIButton) -> Void)? = nil) -> UIButton {
        let deleteButton = UIButton()
        deleteButton.setImage(Asset.delete.image, for: .normal)
        deleteButton.addTarget(self, action: #selector(deleteButtonPressed), for: .touchUpInside)
        
        block?(deleteButton)
        return deleteButton
    }
    
    private func getDoneButton(_ block: ((UIButton) -> Void)? = nil) -> UIButton {
        let doneButton = UIButton()
        doneButton.backgroundColor = ColorName.bondiBlue
        doneButton.cornerRadius(radius: 17.0)
        doneButton.setTitle("Done", for: .normal)
        
        
        let widthConstraint = NSLayoutConstraint(item: doneButton,
                                                 attribute: .width,
                                                 relatedBy: .equal,
                                                 toItem: nil,
                                                 attribute: .notAnAttribute,
                                                 multiplier: 1.0,
                                                 constant: 80.0)
        doneButton.addConstraints([widthConstraint])
        
        block?(doneButton)
        return doneButton
    }
    
    private func getTextView(_ block: ((UITextView) -> Void)? = nil) -> UITextView {
        let textView = UITextView()
        textView.backgroundColor = .clear
        textView.font = UIFont.systemFont(ofSize: 18.0)
        
        view.addSubview(textView, constraints: [
            constraint(\.topAnchor, constant: 24.0),
            constraint(\.leadingAnchor, constant: 24.0),
            constraint(\.trailingAnchor, constant: -24.0),
            constraint(\.bottomAnchor),
        ])
        
        block?(textView)
        return textView
    }
    
    public func alertController(deleteAction: ((UIAlertAction) -> Void)?, cancelAction: ((UIAlertAction) -> Void)?, _ block: ((UIAlertController) -> Void)? = nil) -> UIAlertController {
        let alertController = UIAlertController(title: "Do you want to delete this compulsion?", message: "You cannot undo this action", preferredStyle: .alert)
        let deleteButton = UIAlertAction(title: "Delete", style: .destructive, handler: deleteAction)
        let cancelButton = UIAlertAction(title: "Cancel", style: .default, handler: deleteAction)
        alertController.addAction(cancelButton)
        alertController.addAction(deleteButton)
        
        block?(alertController)
        return alertController
    }
    
}
