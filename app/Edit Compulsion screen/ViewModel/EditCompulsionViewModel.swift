import Foundation
import NOCDKit

protocol EditCompulsionView: BaseAsedoUI {
    
}

class EditCompulsionViewModel: ViewModel {
    
    let view: EditCompulsionView
    let obsessionID: String
    let compulsionID: String
    let delete: DeleteCompulsion
    let edit: EditCompulsion
    
    init(view: EditCompulsionView, obsessionID: String, compulsionID: String, delete: DeleteCompulsion, edit: EditCompulsion) {
        self.view = view
        self.delete = delete
        self.edit = edit
        self.compulsionID = compulsionID
        self.obsessionID = obsessionID
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    private func deleteCompulsion() {
        delete.delete(compulsionID: compulsionID, completion: nil)
    }
    
    private func editCompulsion(text: String) {
        edit.edit(compulsion: text, obsessionID: obsessionID, compulsionID: compulsionID, completion: nil)
    }
    
}
