import UIKit

class EditCompulsionViewController: BaseViewController<EditCompulsionViewModel>, EditCompulsionView {
    
    var deleteButton: UIButton!
    var doneButton: UIButton!
    var textView: UITextView!
    var alertController: UIAlertController!
    var deleteAction: ((UIAlertAction) -> Void)?
    var cancelAction: ((UIAlertAction) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        deleteAction = { _ in
            self.alertController.dismiss(animated: true, completion: nil)
        }
        cancelAction = { _ in
            self.alertController.dismiss(animated: true, completion: nil)
        }
        
        applyDesign()
    }
    
    @objc func deleteButtonPressed() {
        self.present(alertController, animated: true, completion: nil)
    }
    
}
