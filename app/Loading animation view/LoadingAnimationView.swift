import UIKit

class LoadingAnimationView: UIView {

    var activityIndicatorView: UIVisualEffectView!
    var activityIndicator: UIActivityIndicatorView!
    var activityIndicatorLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        applyDesign()
        hideLoader()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func showLoader(text: String = "Loading") {
        UIView.animate(withDuration: 0.25) {
            self.alpha = 1.0
            self.isHidden = false
        }
        activityIndicatorLabel.text = text
    }
    
    public func hideLoader() {
        UIView.animate(withDuration: 0.25) {
            self.alpha = 0.0
            self.isHidden = true
        }
    }
    
    private func applyDesign() {
        backgroundColor = .white
        activityIndicatorView = getActivityIndicatorView({ visualEffectView in
            self.activityIndicator = self.getActivityIndicator(in: visualEffectView)
            self.activityIndicatorLabel = self.getActivityIndicatorLabel(in: visualEffectView)
        })
    }
    
    private func getActivityIndicatorView(_ block: ((UIVisualEffectView) -> Void)? = nil) -> UIVisualEffectView {
        let effectView = UIVisualEffectView(effect: UIBlurEffect(style: .regular))
        effectView.cornerRadius()
        effectView.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(effectView, constraints: [
            constraint(\.centerXAnchor),
            constraint(\.centerYAnchor),
            constraint(\.heightAnchor),
            constraint(\.widthAnchor, \.heightAnchor),
        ])
        
        block?(effectView)
        return effectView
    }
    
    private func getActivityIndicator(in centerView: UIVisualEffectView, _ block: ((UIActivityIndicatorView) -> Void)? = nil) -> UIActivityIndicatorView {
        let activityIndicator = UIActivityIndicatorView(style: .large)
        activityIndicator.startAnimating()
        activityIndicator.color = ColorName.azure
        activityIndicator.hidesWhenStopped = true
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        
        centerView.contentView.addSubview(activityIndicator, constraints: [
            constraint(\.centerXAnchor),
            constraint(\.centerYAnchor),
            constraint(\.heightAnchor, multiplier: 25/100),
            constraint(\.widthAnchor, \.heightAnchor, multiplier: 25/100),
        ])
        
        block?(activityIndicator)
        return activityIndicator
    }
    
    private func getActivityIndicatorLabel(in centerView: UIVisualEffectView, _ block: ((UILabel) -> Void)? = nil) -> UILabel {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12.0)
        label.text = "Authorization..."
        label.textColor = ColorName.azure
        label.translatesAutoresizingMaskIntoConstraints = false
        
        centerView.contentView.addSubview(label, constraints: [
            constraint(\.centerXAnchor),
            constraint(\.bottomAnchor, constant: -8.0)
        ])
        
        block?(label)
        return label
    }
    
}
