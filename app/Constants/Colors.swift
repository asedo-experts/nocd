// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
  import AppKit
  internal enum ColorName { }
#elseif os(iOS) || os(tvOS) || os(watchOS)
  import UIKit
  internal enum ColorName { }
#endif

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// swiftlint:disable identifier_name line_length type_body_length
internal extension ColorName {
  /// 0x1f4b75ff (r: 31, g: 75, b: 117, a: 255)
  static let azure = #colorLiteral(red: 0.121569, green: 0.294118, blue: 0.458824, alpha: 1.0)
  /// 0x212121ff (r: 33, g: 33, b: 33, a: 255)
  static let black = #colorLiteral(red: 0.129412, green: 0.129412, blue: 0.129412, alpha: 1.0)
  /// 0x00a3adff (r: 0, g: 163, b: 173, a: 255)
  static let bondiBlue = #colorLiteral(red: 0.0, green: 0.639216, blue: 0.678431, alpha: 1.0)
  /// 0xff5d47ff (r: 255, g: 93, b: 71, a: 255)
  static let circleRed = #colorLiteral(red: 1.0, green: 0.364706, blue: 0.278431, alpha: 1.0)
  /// 0xffc647ff (r: 255, g: 198, b: 71, a: 255)
  static let circleYellow = #colorLiteral(red: 1.0, green: 0.776471, blue: 0.278431, alpha: 1.0)
  /// 0xf5f5f5ff (r: 245, g: 245, b: 245, a: 255)
  static let gray = #colorLiteral(red: 0.960784, green: 0.960784, blue: 0.960784, alpha: 1.0)
  /// 0x4bb4aeff (r: 75, g: 180, b: 174, a: 255)
  static let turquoiseGreen = #colorLiteral(red: 0.294118, green: 0.705882, blue: 0.682353, alpha: 1.0)
}
// swiftlint:enable identifier_name line_length type_body_length
