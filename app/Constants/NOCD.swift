import UIKit

var appWindow: UIWindow?

let kHierarchyByObsessionIdentifier = "hierarchyByObsession"
let kHierarchyByObsessionAddIdentifier = "hierarchyByObsessionAdd"
let kHierarchyByObsessionSimpleIdentifier = "hierarchyByObsessionSimple"
let kHierarchyByObsessionWithPointIdentifier = "hierarchyByObsessionWithPoint"
let kHierarchyByObsessionWithTitleAndPointIdentifier = "hierarchyByObsessionWithTitleAndPoint"
